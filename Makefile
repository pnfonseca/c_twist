
PROG=example
CC=gcc 
CFLAGS=-Wno-int-conversion

$(PROG): $(PROG).o


.PHONY: clean
clean:	
	rm -f *.o

.PHONY: cleanall
cleanall: clean
	rm -f $(PROG)

.PHONY: run
run:	$(PROG)
	./$(PROG)