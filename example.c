#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "example.h"

#define ARRAYSIZE 012
#define MAXSIZE   800

bool  b_flag;

int main (void)
{
  /**************************************************************************
  *
  *  Variable definitions 
  *
  */

  /* Define integers a and b and their respective pointers, pa and pb */
  int16_t a;
  int16_t b;
  int16_t* pa, pb;

  /* Define onearrray, an array of ARRAYSIZE length */
  int16_t onearray[ARRAYSIZE];
  uint16_t num_el_array;

  /* x, y and z are three integer variables */
  uint16_t x,y,z;

  /**************************************************************************
  *
  *  Executable part
  *
  */

  /* Use function_a to assign a value to the variable a */
  b_flag = false;
  a = function_a();
  printf("a=%d\n",a);

  /* Make pa and pb point to the corresponding variables */
  b = 2;
  pa = &a;
  pb = &b;
  /* Increment both a and b, using the pointers */
  *pa++;
  *pb++;
  printf("a: %d\tb: %d\n",*pa,*pb );

  /* Using onearray, initialize array using init_array()... */
  init_array(onearray);
  
  /* .. and print the array values */
  for(int i=0;i<ARRAYSIZE;i++)
  {
    printf("[%d]: %d\n",i,onearray[i]);
  }


  /* Use x and y in a test to assign a value to z */
  x = 4;
  y = 2;
  if ( x = 5 || y++ < 0 )
  {
    z = 2;
  }
  else
  {
    z = 0;
  }
  printf("x=%d, y=%d, z=%d\n", x,y,z);

  /* Add a closing return char */
  printf('\n');

  return 0;     
}



uint16_t function_a(void)
{
  uint16_t  x=0;    // This value will be returned by the function. 
                    // It is incremented depending on b_flag \
  if (b_flag)
  {
    ++x;
  }

  return x;
}

void init_array(uint16_t *array)
{
  uint16_t i;
  for(i=1;i<=MAXSIZE;i++){
    array[i] = 4;
  }

}